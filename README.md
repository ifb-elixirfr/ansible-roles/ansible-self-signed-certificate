ansible-self-signed-certificate
=======================

This role generates a self signed certificate.

Requirements
------------

None

Role Variables
--------------

| Variable              | Default     | Description                                        |
|-----------------------|-------------|----------------------------------------------------|
| ssl_cert_path         | None        | Self signed certificate path                       |
| ssl_csr_path          | None        | Certificate Signing Request path                   |
| ssl_key_path          | None        | Openssl private key path                           |
| ssl_country           | FR          | Country name associated to the certificate         |
| ssl_state_or_province | Bas-Rhin    | State or province asssociated to the certificate   |
| ssl_locality          | Illkirch    | Locality associated to the certificate             |
| ssl_organization      | POC         | Organization associated to the certificate         |
| ssl_domain            | mydomain.fr | Domain (common name) associated to the certificate |

Dependencies
------------

This role as no dependencies

Example Playbook
----------------


    hosts: all
    vars:
        ssl_cert_path: "/etc/ssl/certs/my_ssl.crt"
        ssl_csr_path: "/etc/ssl/certs/my_ssl.csr"
        ssl_key_path: "/etc/ssl/private/my_ssl.key"
    roles:
        - role: ansible-self-signed-certificate

License
-------

GPL-2.0-or-later

Author Information
------------------

Julien Seiler (@julozi)
